<?php get_header() ?>

    <section class="container" role="main">
        <header class="entry-title">
            <h1>Erro 404</h1>
        </header>

        <div class="entry-content">
            <p>A página não foi encontrada, faça uma busca no site ou entre em contato conosco.</p>
        </div>
    </section>

    <script type="text/javascript">
        document.getElementById('s') && document.getElementById('s').focus()
    </script>

<?php get_footer() ?>
