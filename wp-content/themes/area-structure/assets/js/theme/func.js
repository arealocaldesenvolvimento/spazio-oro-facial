function popupShow(id){
    $('.popup').css('display', 'block');
    $('.popup .perfil .foto img').attr("src", $('#depoimento-'+id+' .foto img').attr("src"));
    $('.popup .perfil .identidade .nome').html($('#depoimento-'+id+' .identidade .nome').html());
    $('.popup .perfil .identidade .cidade').html($('#depoimento-'+id+' .identidade .cidade').html());
    $('.popup .contain .left .texto').html($('#depoimento-'+id+' .texto').html());
    $('.popup .contain .right img').attr("src", $('#depoimento-'+id+' .imagem img').attr("src"));
}
function closePopup(){
    $('.popup').css('display', 'none');
}