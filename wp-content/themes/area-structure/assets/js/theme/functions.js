/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}
window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
}
$(document).ready(()=>{
	var lastScrollTop = 0;
	$(document).scroll(()=>{
		var st = $(document).scrollTop();
		if(st < lastScrollTop && st>100){
			$(".sup-menu-main").css({"z-index" : 9999999999999, "position" : "fixed"});
			$(".sup-menu").css({"z-index" : 9999999999999, "position" : "fixed", "margin-top" : "35px"});
			$("#wrapper").css("padding-top", "194px");
		}else{
			$(".sup-menu-main").css({"z-index" : 9999999999999, "position" : "relative"});
			$(".sup-menu").css({"z-index" : 9999999999999, "position" : "relative", "margin-top" : "0px"});
			$("#wrapper").css("padding-top", "0");
		}
		lastScrollTop = st;
	});
	$('.telefone span input').mask('(00) 0000-0000');
	var sliderDepoimentos = $('#lista-depoimentos').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
		prevHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/prev.png"+'>',
		nextHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/next.png"+'>',
        pager: false
    });
	var sliderDepoimentosMobile = $('#lista-depoimentos-mobile').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
		prevHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/prev.png"+'>',
		nextHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/next.png"+'>',
        pager: false
    });
	var sliderFotos = $('#slider-fotos').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
		prevHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/prev.png"+'>',
		nextHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/next.png"+'>',
        pager: false
    });
	var sliderEquipe = $('#equipe-listagem').lightSlider({
		item: 4,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
		prevHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/prev.png"+'>',
		nextHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/next.png"+'>',
        pager: false
    });
	var sliderEquipeMobile = $('#equipe-listagem-mobile').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
		prevHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/prev.png"+'>',
		nextHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/next.png"+'>',
        pager: false
    });
	var sliderSchrodinger = $('.slider-de-schrodinger').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
		prevHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/prev-2.png"+'>',
		nextHtml: '<img src='+window.alUrl.homeUrl+"/wp-content/themes/area-structure/assets/images/next-2.png"+'>',
        pager: false
    });
	$(".equipe").find(".lSPager").css("margin-right", ((($(".equipe").find(".lSPager").find("li").length)*18)-18)+"px");
	
    $('.load_more a').click(function(event) {
        event.preventDefault();
        var link = $(this).attr('href');
        var text_load = $('.load_more a').html();
        $('.load_more a').html('Carregando...');
        $.get(link, function(data) {
            var post = $(".lista li", data);
            $('.lista').append(post);
            if($('.load_more a', data).attr('href')){
              $('.load_more a').attr('href', $('.load_more a', data).attr('href'));
              $('.load_more a').html(text_load);
            }else{
              $('.load_more').remove();
            }
        });
    });
});