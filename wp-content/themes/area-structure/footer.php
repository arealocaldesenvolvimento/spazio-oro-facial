    </div>

    <!-- Footer -->
    <footer class="main-footer">
        <div class="sup-footer">
            <div class="al-container">
                <ul class="ul-footer">
                    <li class="logo">
                        <img src="<?= get_field("logo", "option")['url'] ?>" alt="" class="logo">
                    </li>
                    <li class="pessoa">
                        <span class="nome">
                            <?= get_field("nome", "option")?><br>
                            <span class="cro"><?= get_field("cro", "option")?></span>
                        </span><br>
                        <div class="redes">
                            <a target="_blank" href="<?= get_field("facebook", "option")?>"><img src="<?= get_image_url("facebook-footer-icon.png") ?>"></a>
                            <a target="_blank" href="<?= get_field("instagram", "option")?>"><img src="<?= get_image_url("instagram-footer-icon.png") ?>"></a>
                        </div>
                    </li>
                    <li class="agenda">
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")["numero_para_sistema"] ?>" class="btn">
                            <img src="<?= get_image_url("calendar-icon.png") ?>" alt="">
                            Agendar sua consulta
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="sub-footer">
            <div class="al-container">
                Desenvolvido por <a href="http://arealocal.com.br/">Área Local</a>
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script src="https://kit.fontawesome.com/4800576786.js" crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"></script>
    <script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/app.js"></script>
    <?php wp_footer(); ?>
</body>
</html>
