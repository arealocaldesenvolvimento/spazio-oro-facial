<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightslider.css">
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightgallery.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?= get_template_directory_uri() ?>/assets/js/libs/lightslider.js"></script>
    <script src="<?= get_template_directory_uri() ?>/assets/js/theme/func.js"></script>
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" class="main-menu">
        <section class="sup-menu">
            <div class="al-container">
                <ul class="contato">
                    <li class="text">
                        <a target="_blank" href="tel: <?= get_field("telefone", "option")['numero_para_sistema'] ?>">
                            <?= get_field("telefone", "option")['numero_visivel'] ?>
                        </a>
                        |
                        <a target="_blank" href="tel: <?= get_field("whatsapp", "option")['numero_para_sistema'] ?>">
                            <?= get_field("whatsapp", "option")['numero_visivel'] ?>
                        </a>
                    </li>
                    <li class="icon">
                        <a target="_blank" href="<?= get_field("facebook", "option") ?>"><img src="<?= get_image_url("facebook-icon.png") ?>" alt="Ícone Facebook"></a>
                    </li>
                    <li class="icon">
                        <a target="_blank" href="<?= get_field("instagram", "option") ?>"><img src="<?= get_image_url("instagram-icon.png") ?>" alt="Ícone Instagram"></a>
                    </li>
                    <li class="icon">
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")["numero_para_sistema"] ?>"><img src="<?= get_image_url("whatsapp-icon.png") ?>" alt="Ícone Whatsapp"></a>
                    </li>
                </ul>
            </div>
        </section>
        <section class="sup-menu-main">
            <div class="al-container">
                <div class="logo">
                    <a title="spazio" href="<?= get_site_url() ?>">
                        <img src="<?= get_field('logo', 'option')['url'] ?>" alt="Logo Spazio">
                    </a>
                </div>
                <div class="menu-lista">
                    <?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
                </div>
            </div>
        </section>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
