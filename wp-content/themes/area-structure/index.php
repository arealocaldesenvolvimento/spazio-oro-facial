<?php get_header() ?>
    <section class="blogs">
        <div class="al-container">
            <h2 class="title">Blog <div class="underline"></div></h2>
            <ul class="lista"> 
                <?php
                    $blog = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page'	=> -1,
                        'order' => 'DESC',
                    ));
                    while($blog->have_posts()):
                        $blog->the_post();
                        ?>
                            <li>
                                <div class="imagem">
                                    <img src="<?= get_field("imagem_thumb")['url']?>" alt="Imagem Thubmnail"> 
                                </div>
                                <h2 class="subtitle"><?= the_title()?></h2>
                                <span class="data"><?= get_the_date() ?></span>
                                <div class="hr"></div>
                                <div class="text">
                                    <?= get_field("texto") ?>
                                </div>
                                <a href="<?= get_post_permalink()?>">Veja Mais</a>
                            </li>
                        <?php 
                    endwhile;
                ?>
            </ul>
        </div>
    </section>
<?php get_footer() ?>