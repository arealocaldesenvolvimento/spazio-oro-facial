<?php get_header() ?>
<?php 
    $filtro = '';
    if(!empty($_GET['filtro'])){
        $filtro = array (
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $_GET['filtro'],
        );
    }else if (!empty(get_queried_object()->term_id)) {
        $filtro = array (
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => get_queried_object()->term_id,
        );
    }
?>
    <section class="blogs">
        <div class="al-container">
            <ul class="lista-categorias">
                <?php 
                    $categorias = get_categories(array(
                        'hide_empty' => false,
                        'orderby' => 'slug',
                        'order' => 'ASC'
                    ));
                    foreach($categorias as $categoria){ ?>
                        <li><a href="<?= get_category_link($categoria->term_id) ?>"><?= $categoria->name ?><div class="underline"></div></a></li>
                    <?php } ?>
            </ul>
            <h2 class="title">Blog <div class="underline"></div></h2>
            <ul class="lista"> 
                <?php
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $blog = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page'	=> 20,
                        'paged' => $paged,
                        'tax_query' => array($filtro)
                    ));
                    while($blog->have_posts()):
                        $blog->the_post();
                        ?>
                            <li>
                                <div class="imagem">
                                    <img src="<?= get_field("imagem_thumb")['url']?>" alt="Imagem Thubmnail"> 
                                </div>
                                <h2 class="subtitle"><?= the_title()?></h2>
                                <span class="data"><?= get_the_date() ?></span>
                                <div class="hr"></div>
                                <div class="text">
                                    <?= get_field("texto") ?>
                                </div>
                                <a href="<?= get_post_permalink()?>">Veja Mais</a>
                            </li>
                        <?php 
                    endwhile;
                ?>
            </ul>
            <?php if($blog->max_num_pages>=20) {?>
                <div class="bottom clearfix centered-button">
                    <nav class="load_more ler-mais">
                        <?php echo get_next_posts_link('Carregar mais!', $blog->max_num_pages); ?>
                    </nav>
                </div>
            <?php } ?>
            </div>
        </div>
    </section>
<?php get_footer() ?>