<?php get_header() ?>
    <section class="min-banner-servicos">
        <img src="<?= the_post_thumbnail_url() ?>" alt="">
    </section>
    <section class="servicos">
        <div class="al-container">
            <h2 class="title">Clareamento Dental <div class="underline"></div></h2>
            <div class="separador">
                <?php foreach(get_field("texto_imagem") as $chave => $textoImagem): ?>
                    <div class="line">
                        <?php if((($chave+1)%2)!=0){ ?>
                            <div class="left <?= (($chave+1)%2)!=0? "order-1":"order-2" ?>">
                                <?= $textoImagem['texto']?>
                            </div>
                            <div class="right <?= (($chave+1)%2)==0? "order-1":"order-2" ?>">
                                <ul class="slider-de-schrodinger">
                                    <?php foreach($textoImagem['imagem'] as $img){ ?>
                                        <li><img src="<?= $img['url'] ?>" alt="<?= $img['title'] ?>"></li>
                                    <?php }?>
                                </ul>
                            </div>
                        <?php }else{ ?>
                            <div class="left <?= (($chave+1)%2)!=0? "order-1":"order-2" ?>">
                                <ul class="slider-de-schrodinger">
                                    <?php foreach($textoImagem['imagem'] as $img){ ?>
                                        <li class="<?= $textoImagem['detalhe']?"detalhe" : 'normal' ?>"><img src="<?= $img['url'] ?>" alt="<?= $img['title'] ?>"></li>
                                    <?php }?>
                                </ul>
                            </div>
                            <div class="right <?= (($chave+1)%2)==0? "order-1":"order-2" ?>">
                                <?= $textoImagem['texto']?>
                            </div>
                        <?php } ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php if(get_field("quadros")!=""){?>
                <div class="quadros implante">
                    <div class="pre-quadros">
                        <?= get_field("pre_quadros") ?>
                    </div>
                    <ul class="quadros-ul">
                        <?php foreach(get_field("quadros") as $chave => $quadro): ?>
                            <li class="quadro">
                                <img src="<?= $quadro['url'] ?>" alt="<?= $quadro['title'] ?>">
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if(get_field("card")!=""){?>
                <div class="cards">
                    <div class="pre-cards">
                        <?= get_field("pre_card") ?>
                    </div>
                    <ul class="cards-ul">
                        <?php foreach(get_field("card") as $chave => $card): ?>
                            <li class="card">
                                <div class="titulo <?= (($chave+1)%2)!=0 ? "amarelo" : "laranja" ?>">
                                    <p><?= $card['titulo'] ?></p>
                                </div>
                                <div class="texto">
                                    <p><?= $card['texto'] ?></p>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if(get_field("listagem")!=""){?>
                <div class="lista">
                    <div class="pre-listagem">
                        <?= get_field("pre_listagem") ?>
                    </div>
                    <ul class="listagem-ul">
                        <?php foreach(get_field("listagem") as $chave => $item): ?>
                            <li class="item">
                                <div class="title">
                                    <?= ($chave+1) ?><br>
                                    <?= $item["titulo"] ?> 
                                </div>
                                <div class="content">
                                    <?= $item["texto"] ?> 
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php } ?>
            <div class="contact">
                <h2 class="title">Agendar uma consulta <div class="underline"></div></h2>
                <?= do_shortcode('[contact-form-7 id="84" title="Contato"]')?>
            </div>
        </div>
    </section>
<?php get_footer() ?>
