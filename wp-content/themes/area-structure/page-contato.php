<?php get_header() ?>
    <section class="min-banner-contato">
        <img src="<?= the_post_thumbnail_url() ?>" alt="">
    </section>
    <section class="contato">
        <div class="al-container">
            <h2 class="title">Contato <div class="underline"></div></h2>
            <div class="separador">
                <div class="left">
                    <div class="text">
                        <p>Esse espaço está aberto para você enviar suas dúvidas, sugestões, críticas e comentários.</p> 
                        <p>Preencha o formulário para que possamos respondê-lo.</p>
                    </div>
                    <div class="headset">
                        <img src="<?= get_image_url("icone-headset.png") ?>" alt="Ícone headset">
                        <a target="_blank" href="tel: <?= get_field("telefone", "option")['numero_para_sistema'] ?>">
                            <?= get_field("telefone", "option")['numero_visivel'] ?>
                        </a>
                        <span>|</span>
                        <a target="_blank" href="tel: <?= get_field("whatsapp", "option")['numero_para_sistema'] ?>">
                            <?= get_field("whatsapp", "option")['numero_visivel'] ?>
                        </a>
                    </div>
                    <div class="mail">
                        <img src="<?= get_image_url("mail-icon.png") ?>" alt="Ícone email">
                        <a target="_blank" href="maito: <?= get_field("e-mail", "option") ?>">
                            <?= get_field("e-mail", "option") ?>
                        </a>
                    </div>
                    <div class="redes">
                        <a target="_blank" href="tel: <?= get_field("facebook", "option") ?>">
                            <img src="<?= get_image_url("facebook-icon-2.png") ?>" alt="Ícone headset">
                        </a>
                        <a target="_blank" href="tel: <?= get_field("instagram", "option") ?>">
                            <img src="<?= get_image_url("instagram-icon-2.png") ?>" alt="Ícone headset">
                        </a>
                        <a target="_blank" href="tel: <?= get_field("whatsapp", "option")['numero_para_sistema'] ?>">
                            <img src="<?= get_image_url("whatsapp-icon-2.png") ?>" alt="Ícone headset">
                        </a>
                    </div>
                </div>
                <div class="right">
                    <?= do_shortcode('[contact-form-7 id="84" title="Contato"]')?>
                </div>
            </div>
        </div>
        <div class="localizacao">
            <div class="al-container">
                <div class="contain">
                    <img src="<?= get_image_url("map-icon.png") ?>" alt="Ícone">
                    <a class="text" href="<?= get_field("localizacao", "option")['url_simples'] ?>" target="_blank">
                        <p class="nome">Spazio OroFacial</p>
                        <p class="localidade"><?= get_field("localizacao", "option")['por_extenso'] ?></p>
                    </a>
                </div>
            </div>
            <?= get_field("localizacao", "option")['iframe'] ?>
        </div>
    </section>
<?php get_footer() ?>