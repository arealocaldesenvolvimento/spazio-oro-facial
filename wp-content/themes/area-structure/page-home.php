
    <section class="popup">
        <a class="close" onclick="closePopup()"><img src="<?= get_image_url('close-icon.png')?>" alt="Ícone de fechar"></a>
        <div class="perfil">
            <div class="foto">
                <img src="<?= $depoimento['foto']['url'] ?>" alt="Foto de perfil">
            </div>
            <div class="identidade">
                <span class="nome"><?= $depoimento['nome']?></span>
            </div>
            <div class="identidade">
                <span class="cidade"><?= $depoimento['localidade']?></span>
            </div>
        </div>
        <div class="contain">
            <div class="left">
                <div class="texto">
                    <?= $depoimento['texto']?>
                </div>
            </div>
            <div class="right">
                <img src="" alt="">
            </div>
        </div>
    </section>
<?php get_header() ?>
    <section class="slider">
        <?= do_shortcode('[rev_slider alias="home"][/rev_slider]')?>
    </section>
    <section class="slider-mobile">
        <?= do_shortcode('[rev_slider alias="Slide-home-mobile"][/rev_slider]')?>
    </section>
    <section class="tratamentos">
        <div class="al-container">
            <h2 class="title">Tratamentos <div class="underline"></div></h2>
            <div class="lista-tratamentos">
                <?php foreach(get_field("paginas_de_tratamento") as $tratamento): ?>
                    <a href="<?= get_site_url().'/'.$tratamento['pagina'] ?>">
                        <div class="tratamento">
                            <div class="img">
                                <img src="<?= $tratamento['imagem']['url']?>" alt="<?= $tratamento['imagem']['title']?>">
                            </div>
                            <h3 class="nome"><?= $tratamento['titulo']?></h3>
                            <div class="texto"><?= $tratamento['texto']?></div>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section class="agendar">
        <div class="left">
            <img src="<?= get_image_url("transformar-sorriso.png") ?>" alt="Sorrisos">
            <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")["numero_para_sistema"] ?>" class="btn">
                <img src="<?= get_image_url("whatsapp-icon.png") ?>" alt="">
                <img src="<?= get_image_url("whatsapp-green-icon.png") ?>" alt="">
                Agendar sua consulta
            </a>
        </div>
        <div class="right">
            <img src="<?= get_image_url("sorriso.png") ?>" alt="Sorrisos">
        </div>
    </section>
    <section class="depoimentos" id="depoimentos">
        <div class="al-container">
            <h2 class="title">Depoimentos <div class="underline"></div></h2>
            <div class="lista-depoimentos">
                <ul id="lista-depoimentos">
                    <?php foreach(get_field("depoimentos") as $chave => $depoimento): ?>
                        <li>
                            <div class="contain" id="depoimento-<?= $chave ?>">
                                <div class="foto">
                                    <img src="<?= $depoimento['foto']['url'] ?>" alt="Foto de perfil">
                                </div>
                                <div class="imagem" style="display: none;">
                                    <img src="<?= $depoimento['imagem']['url'] ?>" alt="Foto">
                                </div>
                                <div class="texto">
                                    <?= $depoimento['texto']?>
                                    <a onclick="popupShow('<?= $chave ?>')">Ler mais <div class="under"></div></a>
                                </div>
                                <div class="identidade">
                                    <span class="nome"><?= $depoimento['nome']?></span>
                                </div>
                                <div class="identidade">
                                    <span class="cidade"><?= $depoimento['localidade']?></span>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="lista-depoimentos-mobile">
                <ul id="lista-depoimentos-mobile">
                    <?php foreach(get_field("depoimentos") as $depoimento): ?>
                        <li>
                            <div class="contain">
                                <div class="foto">
                                    <img src="<?= $depoimento['foto']['url'] ?>" alt="Foto de perfil">
                                </div>
                                <div class="texto">
                                    <?= $depoimento['texto']?>
                                    <a onclick="popupShow('<?= $chave ?>')">Ler mais <div class="under"></div></a>
                                </div>
                                <div class="identidade">
                                    <span class="nome"><?= $depoimento['nome']?></span>
                                </div>
                                <div class="identidade">
                                    <span class="cidade"><?= $depoimento['localidade']?></span>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="ultimasnoticias">
        <div class="al-container">
            <h2 class="title">Ultimas Notícias <div class="underline"></div></h2>
            <div class="noticias">
                <?php
                    $blog = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page'	=> 3,
                        'order' => 'DESC',
                    ));
                    while($blog->have_posts()):
                        $blog->the_post();
                        ?>
                            <div class="noticia">
                                <div class="imagem">
                                    <img src="<?= get_field("imagem_thumb")['url']?>" alt="Imagem Thubmnail"> 
                                </div>
                                <h2 class="subtitle"><?= the_title()?></h2>
                                <span class="data"><?= get_the_date() ?></span>
                                <div class="hr"></div>
                                <div class="text">
                                    <?= get_field("texto") ?>
                                </div>
                                <a href="<?= get_post_permalink()?>">Veja Mais</a>
                            </div>
                        <?php 
                    endwhile;
                ?>
            </div>
        </div>
    </section>
    <section class="agendar-inv">
        <div class="left">
            <img src="<?= get_image_url("sorriso-2.webp") ?>" alt="Sorrisos">
        </div>
        <div class="right">
            <img src="<?= get_image_url("transformar-sorriso-2.png") ?>" alt="Sorrisos">
            <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= get_field("whatsapp", "option")["numero_para_sistema"] ?>" class="btn">
                <img src="<?= get_image_url("whatsapp-icon.png") ?>" alt="">
                <img src="<?= get_image_url("whatsapp-green-icon.png") ?>" alt="">
                Agendar sua consulta
            </a>
        </div>
    </section>
    <section class="visita">
        <div class="al-container">
            <h2 class="title">Venha fazer uma visita <div class="underline"></div></h2>
        </div>
        <?= get_field("localizacao", "option")['iframe'] ?>
    </section>
<?php get_footer() ?>
