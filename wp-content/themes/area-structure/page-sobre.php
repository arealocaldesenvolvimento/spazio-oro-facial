<?php get_header() ?>
    <section class="min-banner">
        <img src="<?= the_post_thumbnail_url() ?>" alt="">
    </section>
    <section class="sobre-nos">
        <div class="al-container">
            <h2 class="title">Sobre nós <div class="underline"></div></h2>
            <div class="separate">
                <div class="left">
                    <?= get_field("sobre_nos") ?>
                </div>
                <div class="right">
                    <div class="lista-container">
                        <ul id="slider-fotos">
                            <?php foreach(get_field('fotos') as $foto): ?>
                                <li>
                                    <img src="<?= $foto['url']?> " alt="Foto">
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="video">
                <?= get_field("iframe_do_video") ?>
            </div>
            <div class="lista">
                <p>Oferecemos todos os tipos de tratamentos, entre eles:</p>
                <ul>
                    <?php foreach(get_field("tratamentos") as $tratamento): ?>
                        <li><?= $tratamento['tratamento'] ?></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <h2 class="title">Nossa equipe <div class="underline"></div></h2>
            <div class="equipe">
                <ul id="equipe-listagem">
                    <?php foreach(get_field("funcionario") as $funcionario): ?>
                        <li>
                            <div class="perfil <?= $funcionario['facebook']=="" && $funcionario['instagram']==""? "disable" : "" ?>">
                                <div class="front">
                                    <div class="foto">
                                        <img src="<?= $funcionario['foto']['url'] ?>" alt="Perfil">
                                    </div>
                                    <h2><?= $funcionario['nome'] ?></h2>
                                    <h3><?= $funcionario['funcao'] ?></h3>
                                    <div class="line"></div>
                                    <h3><?= $funcionario['cro'] ?></h3>
                                </div>
                                <div class="back">
                                    <div class="redes">
                                        <?php if($funcionario['facebook']!="" || $funcionario['instagram']!="") { ?>
                                            <?php if($funcionario['facebook']!=""){ ?> 
                                                <a target="_blank" href="<?= $funcionario['facebook'] ?>"><img src="<?= get_image_url("facebook-footer-icon.png") ?>"></a>
                                            <?php } ?>    
                                            <?php if($funcionario['instagram']!=""){ ?> 
                                                <a target="_blank" href="<?= $funcionario['instagram'] ?>"><img src="<?= get_image_url("instagram-footer-icon.png") ?>"></a>
                                            <?php } ?>  
                                        <?php }else{ ?>
                                            <h2>Sem redes sociais</h2>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="equipe">
                <ul id="equipe-listagem-mobile">
                    <?php foreach(get_field("funcionario") as $funcionario): ?>
                        <li>
                            <div class="perfil">
                                <div class="front">
                                    <div class="foto">
                                        <img src="<?= $funcionario['foto']['url'] ?>" alt="Perfil">
                                    </div>
                                    <h2><?= $funcionario['nome'] ?></h2>
                                    <h3><?= $funcionario['funcao'] ?></h3>
                                    <div class="line"></div>
                                    <h3><?= $funcionario['cro'] ?></h3>
                                </div>
                                <div class="back">
                                    <div class="redes">
                                        <?php if($funcionario['facebook']!="" || $funcionario['instagram']!="") { ?>
                                            <?php if($funcionario['facebook']!=""){ ?> 
                                                <a target="_blank" href="<?= $funcionario['facebook'] ?>"><img src="<?= get_image_url("facebook-footer-icon.png") ?>"></a>
                                            <?php } ?>    
                                            <?php if($funcionario['instagram']!=""){ ?> 
                                                <a target="_blank" href="<?= $funcionario['instagram'] ?>"><img src="<?= get_image_url("instagram-footer-icon.png") ?>"></a>
                                            <?php } ?>  
                                        <?php }else{ ?>
                                            <h2>Sem redes sociais</h2>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </section>
<?php get_footer() ?>