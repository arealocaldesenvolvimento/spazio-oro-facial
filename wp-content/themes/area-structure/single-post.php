<?php get_header() ?>
    <section class="min-banner-single">
        <img src="<?= get_image_url("fundo-single-blog.png") ?>" alt="">
    </section>
    <section class="single">
        <div class="al-container">
            <h2 class="title"><?= the_title()?></h2>
            <h3 class="date"><?= get_the_date()?></h3>
            <div class="imagem">
                <img src="<?= get_field("imagem_thumb")['url']?>" alt="Imagem Thumb">
            </div>
            <div class="texto">
                <?= get_field("texto") ?>
            </div>
            <div class="share">
                <h2 class="title">Compartilhe! <div class="underline"></div></h2>
                <div class="shortcode">
                    <?= do_shortcode("[addtoany]")?>
                </div>
            </div>
            <div class="comentarios">
                <h2 class="title">Comentários <div class="underline"></div></h2>
                <div class="shortcode">
                    <?= do_shortcode("[Fancy_Facebook_Comments]")?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer() ?>